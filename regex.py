import re
phonenumberregex = re.compile(r'\d\d\d-\d\d\d\-\d\d\d\d') #\d is used for digits
mo = phonenumberregex.search('My number is 455-455-7157')# return an object

print(mo.group())

phonenumberregex = re.compile(r'(\d\d\d)-(\d\d\d\-\d\d\d\d)') #\d is used for digits (\d) is used to group them together
mo = phonenumberregex.search('My number is 455-455-7157')# return an object

print(mo.group(1))
print(mo.group(2))
